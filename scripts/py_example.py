#!/usr/bin/env python3

import rclpy
from rclpy.node import Node

from std_msgs.msg import Float64MultiArray
from sensor_msgs.msg import JointState


class PoppyController(Node):
    def __init__(self):
        super().__init__('poppy_controller')

        self.cmd_publisher_ = self.create_publisher(
            Float64MultiArray, '/joint_group_position_controller/commands', 10)

        self.joint_state_subscription_ = self.create_subscription(
            JointState,
            '/joint_states',
            self.joint_state_callback,
            10)

        self.cmd_ = Float64MultiArray()
        for _ in range(25):
            self.cmd_.data.append(0)
        self.joint_names_ = []

        self.up = True
        self.run_timer = self.create_timer(0.1, self.run)

    def run(self):
        if len(self.joint_names_) > 0:
            l_elbow_y_idx = self.joint_index('r_elbow_y')
            if self.up:
                if self.cmd_.data[l_elbow_y_idx] < 1.57:
                    self.cmd_.data[l_elbow_y_idx] = self.cmd_.data[l_elbow_y_idx] + 0.05
                else:
                    self.up = False
            else:
                if self.cmd_.data[l_elbow_y_idx] > 0.0:
                    self.cmd_.data[l_elbow_y_idx] = self.cmd_.data[l_elbow_y_idx] - 0.05
                else:
                    self.up = True

        self.cmd_publisher_.publish(self.cmd_)

    def joint_state_callback(self, msg):
        self.joint_names_ = msg.name
        self.get_logger().info(
            "\nJoints\n\tname: %s\n\tposition: %s" %
            (msg.name, msg.position))

    def joint_index(self, joint_name):
        return self.joint_names_.index(joint_name)


def main(args=None):
    rclpy.init(args=args)

    poppy_controller = PoppyController()

    try:
        rclpy.spin(poppy_controller)
    except KeyboardInterrupt:
        pass
    finally:
        rclpy.shutdown()


if __name__ == '__main__':
    main()
