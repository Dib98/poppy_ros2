import launch
from launch.substitutions import LaunchConfiguration
import launch_ros
import xacro
import os


def generate_launch_description():
    pkg_share = launch_ros.substitutions.FindPackageShare(
        package='poppy_ros2').find('poppy_ros2')
    model_path = os.path.join(pkg_share, 'urdf/poppy_humanoid.urdf.xacro')

    # TODO find a way to pass the disable_gravity_arg value to xacro
    # disable_gravity_arg = launch.actions.DeclareLaunchArgument(
    #     name='disable_gravity', default_value="1",
    #     description='Disable gravity on the robot bodies')

    robot_state_publisher_node = launch_ros.actions.Node(
        package='robot_state_publisher',
        executable='robot_state_publisher',
        parameters=[
            {'robot_description': xacro.process_file(model_path).toxml()}]
    )

    return launch.LaunchDescription([
        robot_state_publisher_node
    ])
