#include <memory>
#include <string_view>

#include <rclcpp/rclcpp.hpp>
#include <sensor_msgs/msg/joint_state.hpp>
#include <std_msgs/msg/float64_multi_array.hpp>

#include <fmt/format.h>

using JointState = sensor_msgs::msg::JointState;
using Float64MultiArray = std_msgs::msg::Float64MultiArray;

class PoppyController : public rclcpp::Node {
public:
    PoppyController() : Node("poppy_controller") {
        cmd_publisher_ = create_publisher<Float64MultiArray>(
            "/joint_group_position_controller/commands", 10);

        joint_state_subscription_ = create_subscription<JointState>(
            "/joint_states", 10,
            [this](JointState::SharedPtr msg) { joint_state_callback(msg); });

        cmd_.data.resize(25, 0);
        cmd_.layout.dim.resize(1);
        cmd_.layout.dim[0].label = "q";
        cmd_.layout.dim[0].size = cmd_.data.size();
        cmd_.layout.dim[0].stride = 1;

        using namespace std::chrono_literals;
        run_timer_ = create_wall_timer(100ms, [this] { run(); });
    }

    void run() {
        if (not joint_names_.empty()) {
            auto l_elbow_y_idx = joint_index("r_elbow_y");
            if (up_) {
                if (cmd_.data[l_elbow_y_idx] < 1.57) {
                    cmd_.data[l_elbow_y_idx] += 0.05;
                } else {
                    up_ = false;
                }
            } else {
                if (cmd_.data[l_elbow_y_idx] > 0.) {
                    cmd_.data[l_elbow_y_idx] -= 0.05;
                } else {
                    up_ = true;
                }
            }
        }

        cmd_publisher_->publish(cmd_);
    }

private:
    void joint_state_callback(const JointState::SharedPtr msg) {
        joint_names_ = msg->name;
        RCLCPP_INFO(get_logger(),
                    fmt::format("\nWheels\n\tname: {}\n\tposition: {}\n",
                                fmt::join(msg->name, ", "),
                                fmt::join(msg->position, ", ")));
    }

    size_t joint_index(std::string_view joint_name) {
        size_t idx{};
        for (const auto& name : joint_names_) {
            if (name == joint_name) {
                return idx;
            }
            ++idx;
        }
        throw std::out_of_range{
            fmt::format("Invalid joint name {}", joint_name)};
    }

    rclcpp::Subscription<JointState>::SharedPtr joint_state_subscription_;
    rclcpp::Publisher<Float64MultiArray>::SharedPtr cmd_publisher_;
    rclcpp::TimerBase::SharedPtr run_timer_;
    std::vector<std::string> joint_names_;

    Float64MultiArray cmd_;
    bool up_{true};
};

int main(int argc, char* argv[]) {
    rclcpp::init(argc, argv);

    auto poppy_controller = std::make_shared<PoppyController>();

    rclcpp::spin(poppy_controller);
    rclcpp::shutdown();

    return 0;
}